package com.kajeet.nnivpnadaptor;
 
import com.kajeet.nnivpnadaptor.net.Conversation;
import com.kajeet.nnivpnadaptor.net.ConversationStarter;
import com.kajeet.nnivpnadaptor.service.VpnConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@SpringBootApplication
public class NniVpnAdaptorApplication {

//	@Autowired
//	VpnConversationService pool;

	public static void main(String[] args) throws Exception {

//		// Informal/Kludge unit tests
//		Conversation conversation = null;
//		try {
//			pool.initializePool();
//		} finally {
//			if (conversation != null) conversation.release();
//
//		}

		SpringApplication.run(NniVpnAdaptorApplication.class, args);
	}

	public static void printResults(Process process) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
	}

}
