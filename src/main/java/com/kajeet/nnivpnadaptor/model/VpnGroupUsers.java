package com.kajeet.nnivpnadaptor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.TreeMap;

public class VpnGroupUsers {

	@JsonProperty("group-name")
	private String groupName;
	@JsonProperty("sentinel-user")
	private String sentinelUser;
	private VpnUserName[] users;

	public VpnGroupUsers(){}
//	public VpnGroupUsers(String groupName, VpnUserName[] users){
//		this.groupName = groupName;
//		this.users = users;
//	}

	public String getSentinelUser() {
		return sentinelUser;
	}

	public void setSentinelUser(String sentinelUser) {
		this.sentinelUser = sentinelUser;
	}

	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public VpnUserName[] getUsers() {
		return users;
	}
	@JsonIgnore
	public Map<String, String> getUserMap () {
		Map<String, String> userMap = new TreeMap<>();
		for  (VpnUserName userName: users){
			userMap.put(userName.getUserName(), userName.getUserName());
		}
		return userMap;
	}

	public void setUsers(VpnUserName[] users) {
		this.users = users;
	}
}
