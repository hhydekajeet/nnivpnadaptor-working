package com.kajeet.nnivpnadaptor.model;

public class VpnUserName {

	private String userName;

	public VpnUserName(String userName){ this.userName = userName;}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
