package com.kajeet.nnivpnadaptor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VpnUserInfo {


	/*
	"ppokolev": {
		"conn_group": "dpinto",
		"prop_autologin": "true",
		"prop_superuser": "true",
		"pvt_google_auth_secret": "LPVPZTKQBL7IYM5G",
		"pvt_google_auth_secret_locked": "false",
		"pvt_password_digest": "56965e2a1a995c74da500088947af11dfa27951cc350d0b97d0633075969c31b",
		"type": "user_compile"
  }
	*/

	private String conn_group;
	private String group_declare;
	private boolean prop_autologin;
	private boolean prop_superuser;
	private String pvt_google_auth_secret;
	private boolean pvt_google_auth_secret_locked;
	private String pvt_password_digest;
	private String type;
	private String c2s_dest_s;
	private String c2s_dest_v;
	private String prop_deny;
	private String def_deny;
	private String prop_autogenerate;
	private String prop_force_lzo;

//	@JsonProperty("group_range.0")
//	private String group_range_zero;
//
//	public String getGroup_range_zero() {
//		return group_range_zero;
//	}
//
//	public void setGroup_range_zero(String group_range_zero) {
//		this.group_range_zero = group_range_zero;
//	}

	public String getConn_group() {
		return conn_group;
	}

	public void setConn_group(String conn_group) {
		this.conn_group = conn_group;
	}

	public boolean isProp_autologin() {
		return prop_autologin;
	}

	public void setProp_autologin(boolean prop_autologin) {
		this.prop_autologin = prop_autologin;
	}

	public boolean isProp_superuser() {
		return prop_superuser;
	}

	public void setProp_superuser(boolean prop_superuser) {
		this.prop_superuser = prop_superuser;
	}

	public String getPvt_google_auth_secret() {
		return pvt_google_auth_secret;
	}

	public void setPvt_google_auth_secret(String pvt_google_auth_secret) {
		this.pvt_google_auth_secret = pvt_google_auth_secret;
	}

	public boolean isPvt_google_auth_secret_locked() {
		return pvt_google_auth_secret_locked;
	}

	public void setPvt_google_auth_secret_locked(boolean pvt_google_auth_secret_locked) {
		this.pvt_google_auth_secret_locked = pvt_google_auth_secret_locked;
	}

	public String getPvt_password_digest() {
		return pvt_password_digest;
	}

	public void setPvt_password_digest(String pvt_password_digest) {
		this.pvt_password_digest = pvt_password_digest;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroup_declare() {
		return group_declare;
	}

	public void setGroup_declare(String group_declare) {
		this.group_declare = group_declare;
	}

	public String getC2s_dest_s() {
		return c2s_dest_s;
	}

	public void setC2s_dest_s(String c2s_dest_s) {
		this.c2s_dest_s = c2s_dest_s;
	}

	public String getC2s_dest_v() {
		return c2s_dest_v;
	}

	public void setC2s_dest_v(String c2s_dest_v) {
		this.c2s_dest_v = c2s_dest_v;
	}

	public String getProp_deny() {
		return prop_deny;
	}

	public void setProp_deny(String prop_deny) {
		this.prop_deny = prop_deny;
	}

	public String getDef_deny() {
		return def_deny;
	}

	public void setDef_deny(String def_deny) {
		this.def_deny = def_deny;
	}

	public String getProp_autogenerate() {
		return prop_autogenerate;
	}

	public void setProp_autogenerate(String prop_autogenerate) {
		this.prop_autogenerate = prop_autogenerate;
	}

	public String getProp_force_lzo() {
		return prop_force_lzo;
	}

	public void setProp_force_lzo(String prop_force_lzo) {
		this.prop_force_lzo = prop_force_lzo;
	}
}
