package com.kajeet.nnivpnadaptor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;
import java.util.TreeMap;

public class VpnUserObject {

	private Map<String, VpnUserInfo> map = new TreeMap<>();

	public Map<String, VpnUserInfo> getMap() {
		return map;
	}

	public void setMap(Map<String, VpnUserInfo> map) {
		this.map = map;
	}

	public void put (String key , VpnUserInfo userInfo) {
		this.map.put(key, userInfo);
	}

}
