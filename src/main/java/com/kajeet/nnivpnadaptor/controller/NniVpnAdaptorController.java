package com.kajeet.nnivpnadaptor.controller;

import com.kajeet.nnivpnadaptor.exception.InvalidInputException;
import com.kajeet.nnivpnadaptor.exception.LoginException;
import com.kajeet.nnivpnadaptor.exception.SystemException;
import com.kajeet.nnivpnadaptor.external.logging.asr.Metric;
import com.kajeet.nnivpnadaptor.external.logging.asr.Performance;
import com.kajeet.nnivpnadaptor.model.*;
import com.kajeet.nnivpnadaptor.service.VpnConversationService;
import com.kajeet.nnivpnadaptor.util.TriFunctionExcept;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.kajeet.nnivpnadaptor.service.ExternalLoggerService;
import com.kajeet.nnivpnadaptor.service.NniVpnAdaptorService;

import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping(NniVpnAdaptorController.URL_SLASH)
public class NniVpnAdaptorController {

	public static final String THIS_SERVICE            = "vpnAdaptor";
	public static final String SPACE                   = " ";
	public static final String ACTION_POST             = "POST";
	public static final String ACTION_DELETE           = "DELETE";
	public static final String ACTION_PUT              = "PUT";
	public static final String ACTION_GET              = "GET";
	public static final String MESSAGE_ROUTES_RETURNED = " routes returned";
	public static final String HTTP_OK                 = "200";
	public static final String HTTP_OK_MESSAGE         = "Success";
	public static final String URL_SLASH               = "/";
	public static final String URL_ROOT                = "";
	public static final String URL_ORG                 = "/kajeet"; ///////////////// PROVISIONAL
	public static final String URL_APP                 = "/vpnadaptor"; ///////////////// PROVISIONAL
	public static final String URL_ORG_ACCESS          = URL_ORG + URL_APP;
	public static final String PATH_VAR_USER           = "user";
	public static final String URL_VAR_USER            = "{user}";
	public static final String URL_APP_USER            = URL_ORG_ACCESS + URL_SLASH + PATH_VAR_USER + URL_SLASH + URL_VAR_USER;
	public static final String PATH_VAR_GROUP          = "group";
	public static final String URL_VAR_GROUP           = "{group}";
	public static final String URL_APP_USER_GROUP      = URL_ORG_ACCESS + URL_SLASH + PATH_VAR_USER + URL_SLASH + URL_VAR_USER
		                                                   + URL_SLASH + PATH_VAR_GROUP + URL_SLASH + URL_VAR_GROUP;
	public static final String URL_APP_GROUP           = URL_ORG_ACCESS + URL_SLASH + PATH_VAR_GROUP + URL_SLASH + URL_VAR_GROUP;

	private static final Logger log = (Logger) LogManager.getLogger(NniVpnAdaptorController.class);
	
	@Autowired
	NniVpnAdaptorService service;

	@Autowired
	ExternalLoggerService loggerService;

	@Autowired
	VpnConversationService pool;

	@GetMapping(URL_ROOT+"/admin")
	public String getConnectionPoolStatus () {
    pool.logConversationStatus();
		return URL_ROOT+"/admin";
	}

	private VpnUserInfoMap callAndLog (String endpoint, String param1, String param2, String action
																		, TriFunctionExcept<String, String, Performance, VpnUserInfoMap> serviceMethodWString
		                                , VpnGroupUsers groupUsers
																		, TriFunctionExcept<String, VpnGroupUsers, Performance, VpnUserInfoMap> serviceMethodWVpnGroupUsers)
		                     throws LoginException, InvalidInputException, SystemException {
		log.info(String.format(THIS_SERVICE + ".callAndLog %s [START]", action));
		Metric metric = new Metric(THIS_SERVICE);
		metric.setEvent(action + SPACE + endpoint);
		Performance callStats = metric.getPerformance();

		LoginException loginException = null;
		InvalidInputException invalidInputException = null;
		SystemException systemException = null;
		VpnUserInfoMap userInfoMap = null;
		try {
			userInfoMap = serviceMethodWVpnGroupUsers!=null?serviceMethodWVpnGroupUsers.apply(param1, groupUsers, callStats)
				          : serviceMethodWString.apply(param1, param2, callStats);
			metric.setResult(HTTP_OK);
			metric.setReason(HTTP_OK_MESSAGE);
		} catch (LoginException x) {
			loginException = x;
			metric.setReason(x.getMessage());
			metric.setResult(HttpStatus.UNAUTHORIZED.toString());
		} catch (InvalidInputException x) {
			invalidInputException = x;
			metric.setReason(x.getMessage());
			metric.setResult(HttpStatus.METHOD_NOT_ALLOWED.toString());
		} catch (SystemException x) {
			systemException = x;
			metric.setReason(x.getMessage());
			metric.setResult(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		}
		metric.setPerformance(callStats);
		loggerService.postLogEntry(metric);
		log.info(String.format(THIS_SERVICE + ".callAndLog %s [END]", action));
		if (loginException != null) throw loginException;
		if (invalidInputException != null) throw invalidInputException;
		if (systemException != null) throw systemException;
		return userInfoMap;
	}

	@GetMapping (URL_ROOT)
	public String welcome() {
		return "Welcome to the Kajeet VPN Adapter!";
	}

	@GetMapping(URL_ORG_ACCESS) // FOR DEV/TESTING PURPOSES FOR DEV/TESTING PURPOSES FOR DEV/TESTING PURPOSES
	public ResponseEntity<Map<String, VpnUserInfo>> testUserInfo() throws LoginException, InvalidInputException, SystemException {
		// VpnUserObject userObject = new VpnUserObject();
		Map<String, VpnUserInfo> map = new TreeMap<>();
		VpnUserInfo userInfo = new VpnUserInfo();
		userInfo.setType("user_connect");
		userInfo.setConn_group("Monsters of the Deep");

		map.put("Howard", userInfo);
		return ResponseEntity.ok().body(map);
	}

	// https://arterra.atlassian.net/browse/WEB-3841
	// GET /kajeet/vpnadaptor/user/{user}
	@GetMapping(URL_APP_USER)
	public ResponseEntity<VpnUserInfoMap> getUserInfo(@PathVariable(PATH_VAR_USER) String userName) throws LoginException, InvalidInputException, SystemException {
    String methodName = "getUserInfo";
		VpnUserInfoMap userInfoMap = callAndLog (URL_APP_USER, userName, null, ACTION_GET, (x,y,z)->service.getUserInfo(x,y,z), null, null);
		return ResponseEntity.ok().body(userInfoMap);
	}

	//https://arterra.atlassian.net/browse/WEB-3839
	// POST /kajeet/vpnadaptor/user/{user}/group/{group}
	@PostMapping(URL_APP_USER_GROUP)
	public ResponseEntity<VpnUserInfoMap> createUserInGroup(@PathVariable(PATH_VAR_USER) String userName, @PathVariable(PATH_VAR_GROUP) String groupName) throws LoginException, InvalidInputException, SystemException {
		VpnUserInfoMap userInfoMap = callAndLog (URL_APP_USER_GROUP, userName, groupName, ACTION_POST, (x,y,z)->service.createUserInGroup(x,y,z), null, null);
		return ResponseEntity.ok().body(userInfoMap);
	}

	// https://arterra.atlassian.net/browse/WEB-3831
	// PUT /kajeet/vpnadaptor/group/{group}
	@PutMapping(URL_APP_GROUP)
	public ResponseEntity<VpnUserInfoMap> auditGroupUsers(@PathVariable(PATH_VAR_GROUP) String groupName, @RequestBody (required = false) VpnGroupUsers payload) throws LoginException, InvalidInputException, SystemException {
//		VpnUserInfoMap returnVal = service.auditGroupUsers(groupName, payload, null);
		VpnUserInfoMap groupUserInfoMap = callAndLog (URL_APP_GROUP, groupName, null, ACTION_GET, null, payload, (x,y,z)->service.auditGroupUsers(x,y,z));
		return ResponseEntity.ok().body(groupUserInfoMap);
	}

	// https://arterra.atlassian.net/browse/WEB-3840
	// DELETE /kajeet/vpnadaptor/user/{user}
	@DeleteMapping(URL_APP_USER)
	public ResponseEntity<VpnUserInfoMap> deleteUser(@PathVariable(PATH_VAR_USER) String userName) throws LoginException, InvalidInputException, SystemException {
		VpnUserInfoMap userInfoMap = callAndLog (URL_APP_USER_GROUP, userName, THIS_SERVICE, ACTION_DELETE, (x,y,z)->service.deleteUser(x,y,z), null, null);
		return ResponseEntity.ok().body(userInfoMap);
	}

	@GetMapping(URL_APP_GROUP)
	// GET /kajeet/vpnadaptor/group/{group}
	public ResponseEntity<VpnUserInfoMap> getGroupUserInfo(@PathVariable(PATH_VAR_GROUP) String groupName) throws LoginException, InvalidInputException, SystemException {
		String methodName = "getGroupUserInfo";
		VpnUserInfoMap groupUserInfoMap = callAndLog (URL_APP_GROUP, groupName, null, ACTION_GET, (x,y,z)->service.getGroupUserInfo(x,y,z), null, null);
		return ResponseEntity.ok().body(groupUserInfoMap);
	}

}

