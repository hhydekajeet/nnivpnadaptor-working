package com.kajeet.nnivpnadaptor.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.kajeet.nnivpnadaptor.controller.NniVpnAdaptorController;
import com.kajeet.nnivpnadaptor.external.logging.asr.Metric;
import com.kajeet.nnivpnadaptor.service.ExternalLoggerService;
 
/**
 * Adapted and stripped down from STaylor's DaylightAPI version by HHyde on 3/8/2021.
 */

@ControllerAdvice()
public class GatewayResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	public static final String GENERAL_EVENT = "NniCliAdaptor GET PUT POST or DELETE";

	private static final Logger log = (Logger) LogManager.getLogger(GatewayResponseEntityExceptionHandler.class);
	//Logger log = LoggerFactory.getLogger(GatewayResponseEntityExceptionHandler.class);
	
	@Autowired
	ExternalLoggerService loggerService;
	
	private void logExternal (String code, String message) {
		Metric metric = new Metric(NniVpnAdaptorController.THIS_SERVICE);
		metric.setEvent(GENERAL_EVENT);
		metric.setResult(code);
		metric.setReason(message);
		loggerService.postLogEntry(metric);
	}

	@ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<GatewayError> handleInvalidInputException(HttpServletRequest req, InvalidInputException ex) {
        GatewayError gatewayError = new GatewayError(HttpStatus.METHOD_NOT_ALLOWED.value(), ex.getLocalizedMessage(), null);
        gatewayError.setFields(ex.getFields());
        log.warn("InvalidInputException: " + gatewayError.toString());
        HttpStatus httpStatus = HttpStatus.METHOD_NOT_ALLOWED;
        logExternal(httpStatus.toString(), gatewayError.toString());
        return new ResponseEntity<>(gatewayError, httpStatus);
    }
    
    @ExceptionHandler(LoginException.class)
    public ResponseEntity<GatewayError> handleLoginException(HttpServletRequest req, LoginException ex) {
        GatewayError gatewayError = new GatewayError(HttpStatus.UNAUTHORIZED.value(), ex.getLocalizedMessage(), null);
        log.warn("LoginException: " + gatewayError.toString());
        HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
        logExternal(httpStatus.toString(), gatewayError.toString());
        return new ResponseEntity<>(gatewayError, httpStatus);
    }

    @ExceptionHandler(SystemException.class)
    public ResponseEntity<GatewayError> handleSystemException(HttpServletRequest req, SystemException ex) {
    	String message = ex.getCause() != null && !StringUtils.isEmpty(ex.getCause().getLocalizedMessage()) ? ex.getCause().getLocalizedMessage() : ex.getLocalizedMessage();
    	int errorCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        GatewayError gatewayError = new GatewayError(errorCode, message, null);
        log.error("SystemException: " + gatewayError.toString());
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        logExternal(httpStatus.toString(), gatewayError.toString());
        return new ResponseEntity<>(gatewayError, httpStatus);
    }
    

    // Example of exception handler that comes with ResponseEntityExceptionHandler
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(),null);
        log.warn("HttpRequestMethodNotSupportedException: " + gatewayError.toString());
        logExternal(status.toString(), gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError, headers, status);
    }
    
    @Override
    protected ResponseEntity<Object>handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.warn("MissingServletRequestParameterException: " + gatewayError.toString());
        logExternal(status.toString(), gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError, headers, status);
    }

    @Override
    protected ResponseEntity<Object>handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String resourceNotFoundMsg = "Resource not found: " + ex.getRequestURL();
        GatewayError gatewayError = new GatewayError(status.value(), resourceNotFoundMsg);
        gatewayError.getFields().add("URL");
        log.warn("NoHandlerFoundException: " + gatewayError.toString());
        logExternal(status.toString(), gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError, headers, status);
    }

    @Override
    protected ResponseEntity<Object>handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.info("MissingPathVariableException: " + gatewayError.toString());
        logExternal(status.toString(), gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError,status);
    }
    
    @Override
    protected ResponseEntity<Object>handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        status = HttpStatus.METHOD_NOT_ALLOWED;
    	GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.warn("TypeMismatchException: " + gatewayError.toString());
        logExternal(status.toString(), gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError,status);
    }

    @Override
    protected ResponseEntity<Object>handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        status = HttpStatus.METHOD_NOT_ALLOWED;
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.warn("HttpMessageNotReadableException: " + gatewayError.toString());
        logExternal(status.toString(), gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError,status);
    }
    
}
