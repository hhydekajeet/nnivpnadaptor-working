package com.kajeet.nnivpnadaptor.exception;

/**
 * Exceptions Messages
 * 
 * @author Lawrence Matta
 */
public class ExceptionMessage {	
    private ExceptionMessage() {}

	//Auth Exception Messages
	public static final String MISSING_CREDENTIALS = "Missing Credentials";
	public static final String MISSING_USERNAME = "Missing Username";
	public static final String MISSING_PASSWORD = "Missing Password";
	public static final String MISSING_SSN = "Missing last 4 SSN";
	public static final String MISSING_TN = "Missing Telephone Number";
	public static final String MISSING_LOOKUP_VALUE = "Missing Lookup value";
	public static final String MISSING_SECRET = "Missing Secret value";
	public static final String AUTH_FAILURE = "Authentication Failure";
	public static final String MISSING_TOKEN = "Missing Token";
	public static final String TOKEN_REFRESH_FAIL = "Token Refresh Failure";
	public static final String ERROR_CIMA_RESP = "Unable to parse CIMA Auth response";
	public static final String ERROR_TOKEN_GEN = "Unable to generate Token";
	public static final String ERROR_ACCOUNT_INFO = "Error getting Account Info";
	public static final String INVALID_AUTH_TOKEN = "Invalid or Missing Auth Token";
	public static final String INVALID_SECRET = "Invalid Secret";
	public static final String ACCESS_DENIED = "Access denied";
	public static final String ERROR_MAC_INFO = "Error getting MAC Address Info";
	
	//MSP Exception Messages
	public static final String MISSING_ADDRESS = "Missing Address";
	public static final String MISSING_ACCESS_TOKEN = "Missing Comcast's Access Token";
	public static final String MISSING_ACCOUNT_GUID = "Missing Comcast's Account GUID";
	public static final String MISSING_USER_GUID = "Missing Comcast's User GUID";
	public static final String MISSING_ID = "Missing ID";
	public static final String INVALID_ID = "Invalid ID";
	public static final String MISSING_LINE_IDS = "Missing Line Ids";
	public static final String MISSING_DOB = "Missing Date Of Birth";
	public static final String MISSING_DEVICES = "Missing device list";
	public static final String MISSING_FIRST_NAME = "Missing Customer's First Name";
	public static final String MISSING_LAST_NAME = "Missing Customer's Last Name";
	public static final String ERROR_GUIDS = "Error getting GUIDs from MSP";
	public static final String ERROR_ELIGIBILITY = "Error getting eligibility check with MSP";
	public static final String ERROR_MSP_LOGIN = "Unable to logging with MSP";
	public static final String INVALID_CREDITCHK_REQ = "Invalid Crdit Check Request";
	public static final String INVALID_FRAUDCHK_REQ = "Invalid Fraud Check Request";
	public static final String INVALID_ADVERSEACTION_REQ = "Invalid Adverse Action Request";
	public static final String ERROR_LINE_INFO = "Error getting Line Status Info";
	public static final String ERROR_CARRIER = "Error getting Carrier";
	public static final String ERROR_ADVERSEACTION_RESP = "Error with adverse action response";
	public static final String INVALID_RAJWT = "Invalid RA-JWT";
	public static final String MISSING_MDN = "Missing MDN";
	public static final String MISSING_CALLBACK_ID = "Missing CallbackId";
	public static final String MISSING_ORDER_ID = "Missing Order Id";
	public static final String MISSING_CHANNEL_ID = "Missing Channel Id";
	public static final String INVALID_LINESTATUS_REQ = "Invalid Line Status create/update Request";
	public static final String ERROR_TIER = "Error getting Customer Tier";
	public static final String MISSING_INVENTORY_SALES = "Missing Inventory Sales";
	public static final String MISSING_TMS_ACCOUNT_UPDATE = "Missing Account Update";
	public static final String ERROR_UPDATING_TMS = "Error updating TMS";
	public static final String MISSING_EXCHANGE = "Missing Exchange Information";
	public static final String ERROR_SECURITY_CODE = "Error processing security code";
	public static final String MISSING_SECURITY_INFO = "Missing SecurityCodeRequest Information";
	public static final String ERROR_GETTING_FRAUD_STATUS = "Error getting fraud session status.";

	//NC Exception Messages
	public static final String MISSING_ACCOUNT_NO = "Missing Account Number";
	public static final String MISSING_ACTIVE_NO = "Missing Activation Number";
	public static final String MISSING_NOTIF_ID = "Missing Notification ID";
	
	// Customer Exception Messages
	public static final String CUSTOMER_NUMBER_INVALID = "Customer Number is invalid";
	public static final String CUSTOMER_NUM_DEVICE_INVALID = "Customer Number or device ID are invalid";
	public static final String INVALID_ADDRESS = "Not a valid Address";
	public static final String COMCAST_TN_MISSING = "Comcast TN is missing";
	public static final String COMCAST_TN_CUSTOMER_NO_MISSING = "Both Comcast TN and Customer number are missing";
    public static final String INVALID_REQUEST_PARAMS = "Invalid Request Param(s)";
	public static final String INVALID_REQUEST_PARAMS_DATE = "Invalid Request Param(s) for start and end dates";
	public static final String RECORD_NOT_FOUND = "The customer number reference could not be found or doesn't exist.";
	public static final String INVALID_DOB = "Invalid date of birth. Format must be YYYY-MM-DD";
    public static final String INVALID_DATE_STRING = "Format of date string is Invalid";
	public static final String INVALID_TERM_CONDITONS = "Required properties are missing from request body ";
	public static final String CUSTOMER_SEARCH_NO_RESULTS = "No customer records found for search/verify criteria";
	public static final String CUSTOMER_VERIFY_MULT_RESULTS = "Multiple customers found for this verification";
	public static final String CUSTOMER_MISSING_ACCT_GUID = "Customer has no Account Guid assigned";
	public static final String CUSTOMER_LINE_REF_NOT_FOUND = "Customer/Line reference not found";
	public static final String CUSTOMER_DEVICE_REF_NOT_FOUND = "Customer/Device reference not found";
	public static final String INVALID_SIM_ICCID = "Invalid SIM ICCID";
	public static final String SPECIAL_ACCOUNT_TYPE_INVALID = "Invalid Special Account Type";
	public static final String SPECIAL_ACCOUNT_TYPE_NOT_FOUND = "Special Account Type not found";
	public static final String INVALID_CREDIT_ID = "Credit Id is invalid";
	public static final String USER_PROFILE_INVALID = "User profile is invalid";
	public static final String INVALID_ACCOUNT_TRANSACTION = "Transaction type not supported.";
	public static final String INVALID_DPP = "Invalid DPP Number, must be numeric and greater than 0.";
	public static final String MISSING_USER_ID = "Missing User Id";
	public static final String INVALID_COMCAST_ACCOUNT_NO = "Invalid Comcast Account Number.";
	public static final String INVALID_HOLDPAYMENT = "Invalid holdPayment value, must be boolean.";
	public static final String NO_PROFILE_FOUND = "No profile found for customer number.";
	public static final String ALL_CONTACT_INFO_IS_EMPTY = "All contact info can't be empty";
	
	//CyberSource
	public static final String DIGITAL_SIGN_ERROR = "Error generating digital signature"; 
	public static final String INVALID_PAYMENT_TOKEN = "Invalid Payment Token"; 
	
	//Credit Card Exception Messages
	public static final String INVALID_CCTOKEN = "Invalid or Missing Credit Card Token"; 
	public static final String INVALID_CCINFO = "Invalid or Missing Credit Card Info for update"; 
	public static final String INVALID_CUST_GUID = "Invalid or Missing  Customer GUID"; 
	public static final String INVALID_EXP_DATE = "Invalid cc expiration date. Format is YYYY-MM"; 
	public static final String CREDIT_CARD_NOT_FOUND = "Credit Card not found"; 
	public static final String CYBERSRC_ERROR = "Error with CyberSource response"; 
	
	//Usage
	public static final String INVALID_CYCLE_OFFSET = "Invalid Cycle Offset, must be number between 0 and %s";
	public static final String INVALID_CYCLE_LIMIT = "Invalid Cycle Limit, must be number between 1 and %s or (cycleLimit - cycleOffset) cannot be greater than %s";
	public static final String INVALID_OFFSET  = "Invalid Offset, number must be greater than 0";
	public static final String INVALID_LIMIT  = "Invalid limit, number must be greater than 0";

	public static final String NO_BILL_CYCLES_FOUND = "No billing cycles found in specified range for customer";

    // Plans
    public static final String INVALID_PLAN_ID = "Not a valid plan ID";
    public static final String NO_PLAN_CATALOG = "No plans available"; 


	// Payment
    public static final String PAYMENT_AMOUNT_MISSING = "No payment amount provided";
    public static final String INVALID_DEVICE_ID = "Device ID invalid or null";
    public static final String INVALID_AMOUNT_NOT_FOUND   = "Invalid amount or no amount was found in the request body";
	public static final String NO_PAYMENTS_FOR_DEVICE_ID = "No payments associated with the device ID could be found";
	public static final String NO_AGREEMENT_ID_FOR_DEVICE_ID = "No agreement id found for this device ID";

    //Notifications Exception Messages
    public static final String INVALID_NOTIF_ID = "Invalid Notification ID";
    public static final String MISSING_CHANNEL = "Missing Channel";
    public static final String NOTIF_REF_NOT_FOUND = "Notification Channel Reference not found";
    public static final String NOTIF_NOT_FOUND = "Notification not found";
    public static final String NOTIF_CONSUMED = "Notification already marked as read";
    public static final String NOTIF_CUST_MISMATCH = "Notification does not belong to customer";
    
    //Lines of Service Exception Messages
    public static final String NO_LOS_FOUND = "No line of service record found";
    public static final String NO_PENDING_PORT = "No pending port details found";
    public static final String MISSING_DISPLAY_NAME = "Missing Display Name";
	public static final String LINE_ID_EMPTY_OR_NULL = "Line ID empty or null";
	public static final String INVALID_ILD_PLAN_ID = "Invalid ILD plandId";
	public static final String NO_ILD_PLAN_FOUND = "No ILD Plan Found for line";
	public static final String INVALID_OR_NULL_OBJECT = "Invalid or null object";
    public static final String INVALID_LINE_ID = "Invalid line Id";
    public static final String NO_ACTIVE_LOS_FOUND = "No active lines found for customer";
    public static final String LINE_MUST_BE_ACTIVE = "Line must be active in order to perform this operation";
    public static final String NO_DEVICE_INSURANCE_FOUND = "No insurance plan details found for this device";
    public static final String NO_DEVICE_SWITCH_FOUND = "No device switch found for this line of service";
    public static final String INVALID_PORT_RETRY_REQUEST = "Invalid porting retry request";
    public static final String INVALID_STATUS_CANCEL_ACTIVATION = "The actual status of the current activation is not ER (Error)";
    public static final String INVALID_NUMBER_TYPE = "The Number Type was not found or is empty in DB";

    //undosuspend
    public static final String INVALID_REQUEST_FOR_UNSUSPEND_LINE = "CAN NOT UNSUSPEND LINE- REASON - NON PAYMENT";
    
	//Orders
    public static final String INVALID_ORDER_ID = "Invalid OrderId";
    public static final String MISSING_REQUIRED_FIELDS = "Missing or invalid required fields";
	public static final String ORDER_NOT_FOUND = "Order not found";
	public static final String ORDER_DEVICE_NOT_FOUND = "Device Order item not found";
	public static final String ORDER_CANT_BE_CANCELLED = "Order can't be cancelled";
    public static final String NULL_CUST_NO = "Path variable Customer Number Is Null";
    public static final String ORDER_CANCEL_FAIL_ORDER_SHIPPED = "Order has been shipped, it cannot be cancelled";
    public static final String INVALID_EXCHANGE_ID = "ExchangeId is empty or invalid";
    public static final String EXCHANGE_ORDER_NOT_FOUND_FOR_ID = "Could not find Order Exchange item for exchangeId: %s";
    public static final String NO_SHIP_METHODS_FOUND = "No shipping methods found";
    public static final String ORDER_STATUS_UPDATE_FAIL = "Order status could not be updated";
    public static final String INVALID_RETAIL_ORDER = "Invalid retail order";
    public static final String INVALID_ZIP_CODE = "Invalid zip code. Must be 5, 9, or 5-4 digits.";

    //DPP
	public static final String MISSING_LINEID = "Missing lineId";
    public static final String MISSING_DPPID = "Missing dppId";
    public static final String MISSING_IMEI = "Missing imei";
    public static final String MISSING_DEVICEGRADE = "Missing deviceGrade";
    public static final String MISSING_DPPBALANCE = "Missing dppBalance";
    public static final String MISSING_DPPACTION = "Missing deviceAction";

    //RMA
    public static final String INVALID_RMA_ID = "Invalid RMA Id";
    
    //Rates
    public static final String INTL_RATE_NOT_FOUND = "International Rate not found";
    
    //Porting
    public static final String INVALID_PORT_TN = "Invalid Porting Number";
    
    //Account Settings
    public static final String INVALID_DATA_TH = "Invalid Data Limit Threshold, must be numeric and greater than 0";
    
    //Zendesk Exception Message
    public static final String INVALID_ISSUE = "Missing or invalid issue request";
    
    //Generics
    public static final String INVALID_SEARCH = "Missing or invalid search/verify criteria";
    public static final String INVALID_STATUS = "Missing or invalid status";
    
    //AuditTrail
    public static final String INVALID_AUDIT_ENTRY = "Invalid Audit Trail Entry";
    public static final String INVALID_HTTP_REQUST = "Invalid HTTP request";
    
    //EventsLog
    public static final String INVALID_EVENT_LOG_TYPE = "Invalid Event Log Type";
    public static final String INVALID_EVENT_LOG_DATA = "Invalid Event Log Meta Data";

	//Statements
	public static final String MONTH_KEY_NOT_FORMATTED = "Month Key is not formatted properly";
    public static final String NO_STATEMENT_FOUND_MONTH_KEY = "No statement found for month key";
    public static final String NO_STATEMENT_FOR_SEARCH_KEY = "Statement not found for cust_no and search key";
    public static final String INVALID_TRANSACTION_TYPE = "Invalid transaction type";
    public static final String NO_ESTIMATED_STATEMENT = "Customer does not have an estimated statement";
    
    //System
    public static final String GET_APP_NOT_RETURN = "Get application method did not return application with id ";
    public static final String UPDATE_APP_FAILED_INSERT_VALUES = "Failed to insert values into the application with id ";
    public static final String GET_APP_LIST_FAILED = "Get applications failed.";
    public static final String GET_APP_LIST_NULL = "Get applications is null.";
    public static final String APP_REQUEST_PARAMETERS_NULL = "Application request parameters are null or empty.";
    public static final String APP_ID_IS_NULL = "Application's Id is null.";
    public static final String INSERT_APP_FAILED = "Insert application failed";
    public static final String APP_NOT_FOUND = "Invalid application id ";
    public static final String DUPLICATE_KEY = "Records must be unique ";
    public static final String INVALID_PARAMETERS = "Invalid Parameters";
    public static final String INVALID_REQUEST_BODY = "Request body missing";
    public static final String NO_RECORDS_FOUND = "Record Not Found";
    public static final String INVALID_IP = "Invalid Parameters: Not valid IP";
    public static final String INVALID_KEY_NAME = "Invalid Key Name";
    public static final String KEY_ALREADY_DISABLED = "Key already disabled";
    public static final String INVALID_KEY = "Invalid Key Id";
    
    //Product
    public static final String COMPATIBLE_PRODUCT_NOT_FOUND = "Compatible products not found";
    public static final String INVALID_LANGID = "Invalid LangId";
    public static final String INVALID_VALUE_LANGID = "Invalid value for langId";
}
