package com.kajeet.nnivpnadaptor.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by staylor on 8/14/15.
 */
public class InvalidInputException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -916016762492026789L;
	private List<String> fields = null;
	
    public InvalidInputException(String message) {
        super(message);
        this.fields = new ArrayList<String>();
    }
    
    public InvalidInputException(String message, List<String> fields) {
        super(message);
        this.fields = fields;
    }

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}
}
