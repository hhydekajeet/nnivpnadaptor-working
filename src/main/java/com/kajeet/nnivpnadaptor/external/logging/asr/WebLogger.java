package com.kajeet.nnivpnadaptor.external.logging.asr;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.kajeet.nnivpnadaptor.service.ExternalLoggerService;

@Service
public class WebLogger implements ExternalLoggerService {
	
	public static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
	
    @Value("${kajeet.nni.ssh.external.logging.url}")
    private String url = "https://dt1yazj6tj.execute-api.us-east-1.amazonaws.com/dev/kajeet/nni/metric";
		
    @Value("${kajeet.nni.ssh.external.logging.apikey}")
    private String apikey = "A46Iqn9SvJ3ANlV44FKG157VrEwLCRFMaY0Y5BmD";
		

	@Override
	public void postLogEntry(Metric metric) {
		RestTemplate envelope = new RestTemplate();	
		HttpHeaders headers = new HttpHeaders();
		headers.set("x-api-key", apikey);    
		HttpEntity<Metric> request = new HttpEntity<>(metric, headers);	
		ResponseEntity<String> result = envelope.postForEntity(url, request, String.class);
	}

	@Override
	public void postLogEntry (String log_timestamp, String service, String event, String result, String reason,
			String pgw_received, String request_received, String asr_sent, String asr_completed, Long pgw_duration) {
		Performance performance = new Performance(pgw_received, request_received, asr_sent, asr_completed, pgw_duration);
		Metric metric = new Metric(log_timestamp, service, event, result, reason, performance);
		postLogEntry(metric);
	}
	
	public static String nowTimestamp() {
		return LocalDateTime.now().format(DATE_TIME_FORMATTER);
	}


}
