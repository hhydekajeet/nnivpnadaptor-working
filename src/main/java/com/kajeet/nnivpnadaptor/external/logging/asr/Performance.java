package com.kajeet.nnivpnadaptor.external.logging.asr;

//import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


public class Performance {
    private String pgw_received;
    private String request_received = WebLogger.nowTimestamp();
    private String asr_sent;
    private String asr_completed;
    private Long pgw_duration;

    public String getPgw_received() {
        return pgw_received;
    }

    public void setPgw_received(String pgw_received) {
        this.pgw_received = pgw_received;
    }

    public String getRequest_received() {
        return request_received;
    }

    public void setRequest_received(String request_received) {
        this.request_received = request_received;
    }

    public String getAsr_sent() {
        return asr_sent;
    }

    public void setAsr_sent(String asr_sent) {
        this.asr_sent = asr_sent;
    }

    public String getAsr_completed() {
        return asr_completed;
    }

    public void setAsr_completed(String asr_completed) {
        this.asr_completed = asr_completed;
    }

    public Long getPgw_duration() {
        return pgw_duration;
    }

    public void setPgw_duration(Long pgw_duration) {
        this.pgw_duration = pgw_duration;
    }

    public Performance() {}
    public Performance(String pgw_received, String request_received, String asr_sent, String asr_completed,	Long pgw_duration) {
		super();
		this.pgw_received = pgw_received;
		this.request_received = request_received;
		this.asr_sent = asr_sent;
		this.asr_completed = asr_completed;
		this.pgw_duration = pgw_duration;
	}

	@Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
        return objectMapper.writeValueAsString(this);   
        } catch (JsonProcessingException x) {
        	return "JsonProcessingException";
        }
    }
}
