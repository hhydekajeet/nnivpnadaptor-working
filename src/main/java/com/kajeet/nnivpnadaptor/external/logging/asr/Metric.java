package com.kajeet.nnivpnadaptor.external.logging.asr;

//import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Metric {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String log_timestamp = WebLogger.nowTimestamp();
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String service;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String event;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String result;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String reason;
    private Performance performance = new Performance();

    public Metric(String service) {
		super();
		this.service = service;
	}

    public Metric(String log_timestamp, String service, String event, String result, String reason,
			Performance performance) {
		super();
		this.log_timestamp = log_timestamp;
		this.service = service;
		this.event = event;
		this.result = result;
		this.reason = reason;
		this.performance = performance;
	}

	public String getLog_timestamp() {
        return log_timestamp;
    }

    public void setLog_timestamp(String log_timestamp) {
        this.log_timestamp = log_timestamp;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Performance getPerformance() {
        return performance;
    }

    public void setPerformance(Performance performance) {
        this.performance = performance;
    }

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
        return objectMapper.writeValueAsString(this);   
        } catch (JsonProcessingException x) {
        	return "JsonProcessingException";
        }
    }
}
