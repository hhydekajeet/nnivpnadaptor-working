package com.kajeet.nnivpnadaptor.net;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.kajeet.nnivpnadaptor.exception.SystemException;
import com.kajeet.nnivpnadaptor.service.VpnConversationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ConversationPool implements VpnConversationService {

  public static final String CLASS_NAME          = "ConversationPool";
  public static final String DOT                 = ".";
  private final Logger log = (Logger) LogManager.getLogger(ConversationPool.class);
  public final String STRICT_HOSTKEY_CHECKING    = "StrictHostKeyChecking";
  public final String STRICT_HOSTKEY_CHECKING_NO = "no";
  public final String SHELL                      = "shell";
  public final String IN_GET_NEW_CONVERSATION    = "in net.Conversation.getNewConversation(): ";
  public final String JSCH_EXCEPTION_MESSAGE     = "JSchException " + IN_GET_NEW_CONVERSATION;
  public final String IO_EXCEPTION_MESSAGE       = "IOException " + IN_GET_NEW_CONVERSATION;
  public final String OTHER_EXCEPTION_MESSAGE    = "Unanticipated Exception " + IN_GET_NEW_CONVERSATION;
  public static String SPACE                     = " ";
  public static final String START               = " [START] ";
  public static final String END                 = " [END] ";
  public static final String OPEN_PAREN          = "(";
  public static final String CLOSE_PAREN         = ")";
  public static final String THREAD_SLEEP        = "Thread.sleep";
  public static final String TIMESTAMP_FORMAT    = "yyyy-MM-dd HH:mm:ss.SSS";
  public static final String PROMPT_POUND_SIGN   = "#";
  public static final String AT_SIGN             = "@";
  public static final String ROOT_USER           = "root";
  public static final String ROOT_AT             = "root@";


  @Value("${kajeet.nni.ssh.target.host}")
  private String host = "10.101.1.5";

  @Value("${kajeet.nni.ssh.target.user}")
  private String user = "UserMyUser";

  @Value("${kajeet.nni.ssh.target.pass}")
  private String pass = "CaptainMyCaptain";

  @Value("${kajeet.nni.ssh.target.port}")
  private int port = 42;

  @Value("${kajeet.nni.ssh.prompt.magic-char}")
  private String promptMagicChar = "$";

  @Value("${kajeet.nni.ssh.connection.pool.size}")
  private int poolSize = 4;

  private int retries = 3;

  private Map<String, Map<Integer, Conversation>> cache = new TreeMap<>();
  private Map<String, Integer> tokenMap = new TreeMap<>();

  @PostConstruct
  public void init() throws SystemException {
    String methodName = "init";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    // DEV-ONLY TESTING
    log.trace ("sudo /usr/local/openvpn_as/scripts/sacli UserPropGet.indexOf(\"#sudo\"):");
    log.trace ("sudo /usr/local/openvpn_as/scripts/sacli UserPropGet".indexOf("#sudo"));
    // END DEV-ONLY TESTING

    ConversationStarter greeting = new ConversationStarter(user, pass, host, port);
    initializePool(greeting);
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
  }

  @Override
  public int getPoolSize() { return poolSize;}

  @Override
  public void logConversationStatus() {
    Collection<Map<Integer, Conversation>> collection = cache.values();
    for (Map<Integer, Conversation> conversationMap: collection) {
      for ( Integer index: conversationMap.keySet()) {
        log.info("Conversation " + index + ": " + conversationMap.get(index) + " " + (conversationMap.get(index).isInUse()?"inUse":"Not inUse"));
      }
    }

  }

  @Override
  public void initializePool (ConversationStarter greeting) throws SystemException {
    String methodName = "initializePool";
    log.info(CLASS_NAME + DOT+ methodName+OPEN_PAREN + greeting.getUsername() + CLOSE_PAREN + START);
    Map<Integer, Conversation> conversationMap = new TreeMap<>();
    ExecutorService service = Executors.newFixedThreadPool(poolSize);
    try {
      for (int i = 0; i < poolSize; i++) {
        Integer index = i;
        service.submit(() -> {
          Conversation myConversation = getNewConversation(greeting, retries);
          conversationMap.put(index, myConversation);
        });
      }
      cache.put(greeting.toString(), conversationMap);
      tokenMap.put(greeting.toString(), Integer.valueOf(0));
    } finally {
      if (service != null) service.shutdown();
    }
    log.info(CLASS_NAME + DOT+ methodName+OPEN_PAREN + greeting.getUsername() + CLOSE_PAREN + END);
  }


  @Override
  public Conversation getConversation  (ConversationStarter greeting, int numRetries) throws SystemException {
    String methodName = "getConversation";
    log.info(methodName + SPACE + START);
    Map<Integer, Conversation> conversationMap = cache.get(greeting.toString());
    Conversation conversation = null;
    if (conversationMap != null) {
      int poolToken = tokenMap.get(greeting.toString());
      int safetyValve = 0;
      boolean needFreshFace = false;
      do {
        safetyValve++;
        poolToken++;
        needFreshFace = false;
        if (poolToken >= poolSize) {
          poolToken = 0; // Round-robin
          try {Thread.sleep(1000);} catch (InterruptedException x) {}
        }

        log.trace("conversation = conversationMap.get(" + poolToken + ");");
        conversation = conversationMap.get(poolToken);
        log.trace("conversation = " + conversation);

        if (!conversation.isInUse()) {
          // Make sure it's a working session; otherwise scrap it and get a new, shiny one.
          if (!conversation.getSession().isConnected()) needFreshFace = true;
          try {
            conversation.getSession().sendKeepAliveMsg();
          } catch (Exception x) {
            log.error("conversation.getSession().sendKeepAliveMsg() threw exception.");
            needFreshFace = true;
          }
          if (needFreshFace) {
            log.error("Getting new connection to replace dead one in pool...");
            conversation.close();
            conversation = getNewConversation(greeting, retries);
            conversationMap.put(poolToken, conversation);
          }
          conversation.setInUse();
          tokenMap.put(greeting.toString(), poolToken);
          log.info(methodName + SPACE + END);
          return conversation;
        }
        if (safetyValve > 100) throw new SystemException("Unable to connect to OpenVPN host (ran out of available connections). Please try again later.");
      } while (true);
    } else {
      initializePool(greeting);
      log.info(methodName + SPACE + END);
      return getConversation(greeting, retries);
    }
  }

  private Conversation getNewConversation (ConversationStarter greeting, int numRetries) {
    String methodName = "getNewConversation[1]";
    return getNewConversation (greeting.username, greeting.password, greeting.host, greeting.port, numRetries);
  }

  private Conversation getNewConversation (String username, String password, String host, int port, int numRetries) {
    String methodName = "getNewConversation[2]";
    Conversation conversation = null;
    for (int tries=0; tries<numRetries; tries++) {
      conversation = new Conversation();
      conversation.setPromptChar(promptMagicChar);
      try {
        try {
          conversation.session = new JSch().getSession(username, host, port);
          conversation.session.setPassword(password);
          conversation.session.setConfig(STRICT_HOSTKEY_CHECKING, STRICT_HOSTKEY_CHECKING_NO);
          conversation.session.connect(); // May throw up
          conversation.channel = (ChannelShell) conversation.session.openChannel(SHELL);
          conversation.stream = new PrintStream(conversation.channel.getOutputStream());
          conversation.channel.setOutputStream(conversation.responseStream);
          conversation.channel.connect();
          ByteArrayOutputStream responseStream = conversation.getResponseStream();
          String response = null;
//          int indexOfAt = -1;
          int indexOfUserAt = -1;
          int indexOfRootAt = -1;

          // Test if user is root, if so set prompt char
          for (int i=0; i<100; i++){
            log.trace(THREAD_SLEEP+OPEN_PAREN+"100"+CLOSE_PAREN+SPACE+CLASS_NAME+DOT+methodName+"[1]");
            Thread.sleep(100);
            response = new String(responseStream.toByteArray());
//            indexOfAt = response.lastIndexOf(AT_SIGN);
//            if (indexOfAt<0) continue;
            indexOfUserAt = response.indexOf(user+AT_SIGN);
            if (indexOfUserAt>=0) {
              conversation.setPromptPhrase(user+AT_SIGN);
              break;
            }
            indexOfRootAt = response.indexOf(ROOT_AT);
            if (indexOfRootAt>=0) {
              conversation.setPromptChar(PROMPT_POUND_SIGN);
              conversation.setPromptPhrase(ROOT_AT);
              break;
            }
          }

          log.trace("Very first response from host server is: " + responseStream.toString());
          responseStream.reset();
          break;
        } catch (JSchException x) {
          log.error(JSCH_EXCEPTION_MESSAGE + x.getMessage());
        } catch (IOException x) {
          log.error(IO_EXCEPTION_MESSAGE + x.getMessage());
        } catch (Exception x) {
          log.error(OTHER_EXCEPTION_MESSAGE + x.getMessage());
        }
        if (conversation != null) conversation.release();
        conversation = null;
        log.trace(THREAD_SLEEP+OPEN_PAREN+"100"+CLOSE_PAREN+SPACE+CLASS_NAME+DOT+methodName+"[2]");
        Thread.sleep(1000);
      } catch (InterruptedException x) {}
    }
    return conversation;
  }

}
