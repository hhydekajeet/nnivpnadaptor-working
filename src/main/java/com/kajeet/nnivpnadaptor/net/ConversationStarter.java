package com.kajeet.nnivpnadaptor.net;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConversationStarter {
	
	protected String username;
	protected String password;
	protected String host;
	protected int port;
	private String BAR = "|";

	
	public ConversationStarter(String username, String password, String host, int port) {
		super();
		this.username = username;
		this.password = password;
		this.host = host;
		this.port = port;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public boolean equals (Object other) {
		if (!(other instanceof ConversationStarter))	return false;
		ConversationStarter otherConversationStarter = (ConversationStarter)other;
		
		return ((this.username.equals(otherConversationStarter.username))
			&&(this.password.equals(otherConversationStarter.password))
			&&(this.host.equals(otherConversationStarter.host))
			&&(this.port == otherConversationStarter.port)
		);
	}
	@Override
	public String toString(){
		return this.getUsername()+BAR+this.getHost()+BAR+this.getPort();
	}
	
}
