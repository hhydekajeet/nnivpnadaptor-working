package com.kajeet.nnivpnadaptor.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Conversation {

	protected Session session = null;
	protected ChannelShell channel = null;
	protected ByteArrayOutputStream responseStream   = new ByteArrayOutputStream();
	protected PrintStream stream;
	protected String promptChar = "$";
	protected String promptPhrase = null;
	protected ConversationStarter greeting;
	private boolean inUse = false;

	private static final Logger log = (Logger) LogManager.getLogger(Conversation.class);
	public static final String STRICT_HOSTKEY_CHECKING    = "StrictHostKeyChecking"; // CONSIDER REMOVING TO INDEPENDENT UTIL/CONSTANTS CLASS
	public static final String STRICT_HOSTKEY_CHECKING_NO = "no";
	public static final String SHELL                      = "shell";
	public static final String IN_GET_NEW_CONVERSATION    = "in net.Conversation.getNewConversation(): ";
	public static final String JSCH_EXCEPTION_MESSAGE     = "JSchException " + IN_GET_NEW_CONVERSATION;
	public static final String IO_EXCEPTION_MESSAGE       = "IOException " + IN_GET_NEW_CONVERSATION;
	public static final String OTHER_EXCEPTION_MESSAGE    = "Unanticipated Exception " + IN_GET_NEW_CONVERSATION;

	public String getPromptPhrase() {
		return promptPhrase;
	}

	public void setPromptPhrase(String promptPhrase) {
		this.promptPhrase = promptPhrase;
	}

	public String getPromptChar () {return promptChar;}
  public void setPromptChar (String promptChar) {this.promptChar = promptChar;}
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public ChannelShell getChannel() {
		return channel;
	}
	public void setChannel(ChannelShell channel) {
		this.channel = channel;
	}
	public ByteArrayOutputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(ByteArrayOutputStream responseStream) {
		this.responseStream = responseStream;
	}
	public PrintStream getStream() {
		return stream;
	}
	public void setStream(PrintStream stream) {
		this.stream = stream;
	}

	public boolean isInUse() {
		return inUse;
	}
	public void setInUse() {
		this.inUse = true;
	}

	public void release() {
		this.inUse = false;
	}

	public void close () {
		if (this.session != null) {
			this.session.disconnect();
		}
		if (this.channel != null) {
			this.channel.disconnect();
		}
	}

}
