package com.kajeet.nnivpnadaptor.service;

import com.kajeet.nnivpnadaptor.external.logging.asr.Metric;

public interface ExternalLoggerService {
	
	void postLogEntry (Metric metric);
	void postLogEntry (String log_timestamp, String service, String event, String result, String reason,
			String pgw_received, String request_received, String asr_sent, String asr_completed, Long pgw_duration);

}
