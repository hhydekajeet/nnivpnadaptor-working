package com.kajeet.nnivpnadaptor.service;

import com.kajeet.nnivpnadaptor.exception.LoginException;
import com.kajeet.nnivpnadaptor.exception.SystemException;
import com.kajeet.nnivpnadaptor.net.Conversation;
import com.kajeet.nnivpnadaptor.net.ConversationStarter;

public interface VpnConversationService {
	
  void initializePool (ConversationStarter greeting) throws SystemException;
  Conversation getConversation (ConversationStarter greeting, int numRetries) throws SystemException;
  void logConversationStatus();
  int getPoolSize();


}
