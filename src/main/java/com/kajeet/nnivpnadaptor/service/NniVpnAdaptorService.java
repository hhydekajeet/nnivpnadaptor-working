package com.kajeet.nnivpnadaptor.service;

import com.kajeet.nnivpnadaptor.exception.InvalidInputException;
import com.kajeet.nnivpnadaptor.exception.LoginException;
import com.kajeet.nnivpnadaptor.exception.SystemException;
import com.kajeet.nnivpnadaptor.external.logging.asr.Performance;
import com.kajeet.nnivpnadaptor.model.VpnGroupUsers;
import com.kajeet.nnivpnadaptor.model.VpnUserInfoMap;

public interface NniVpnAdaptorService {
	
//  String getServerInfo(); // FOR DEV/TESTING PURPOSES ONLY; PLUG IT BEFORE PRODUCTION

  VpnUserInfoMap getUserInfo(String userName, String dummy, Performance callStats) throws LoginException, InvalidInputException, SystemException;

  VpnUserInfoMap createUserInGroup(String userName, String groupName, Performance callStats) throws LoginException, InvalidInputException, SystemException;

  VpnUserInfoMap auditGroupUsers(String groupName, VpnGroupUsers payload, Performance callStats) throws LoginException, InvalidInputException, SystemException;

  VpnUserInfoMap deleteUser(String userName, String dummy, Performance callStats) throws LoginException, InvalidInputException, SystemException;

  VpnUserInfoMap getGroupUserInfo(String groupName, String dummy, Performance callStats) throws LoginException, InvalidInputException, SystemException;

}
