package com.kajeet.nnivpnadaptor.host;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kajeet.nnivpnadaptor.exception.InvalidInputException;
import com.kajeet.nnivpnadaptor.exception.LoginException;
import com.kajeet.nnivpnadaptor.exception.SystemException;
import com.kajeet.nnivpnadaptor.external.logging.asr.Performance;
import com.kajeet.nnivpnadaptor.external.logging.asr.WebLogger;
import com.kajeet.nnivpnadaptor.model.*;
import com.kajeet.nnivpnadaptor.net.Conversation;
import com.kajeet.nnivpnadaptor.net.ConversationStarter;
import com.kajeet.nnivpnadaptor.service.NniVpnAdaptorService;
import com.kajeet.nnivpnadaptor.service.VpnConversationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class NniVpnAdaptorHostService implements NniVpnAdaptorService {

  private static final Logger log                    = (Logger) LogManager.getLogger(NniVpnAdaptorHostService.class);
  public static final String CLASS_NAME              = "NniVpnAdaptorHostService";
  public static final String CLASS_NAME_DOT          = CLASS_NAME + ".";
  public static String SPACE                         = " ";
  public static int ZERO                             = 0;
  public static int ONE                              = 1;
  public static int TWO                              = 2;
  public static int THREE                            = 3;
  public static int FIVE                             = 5;
  public static int TEN                              = 10;
  public static int FIFTY                            = 50;
  public static int SIXTY                            = 60;
  public static int ONE_HUNDRED                      = 100;
  public static int TWO_HUNDRED                      = 200;
  public static int FIVE_HUNDRED                     = 500;
  public static int ONE_THOUSAND                     = 1000;
  public static final String EQUALS                   = " = ";
  public static final String START                   = " [START] ";
  public static final String END                     = " [END] ";
  public static final String WAIT_FOR_PROMPT         = "waitForPrompt()";
  public static final String MORE_CONTINUATION       = " ...[MORE]... ";
  public static final int WAIT_TIMEOUT_GET           = ONE_HUNDRED;
  public static final String SUDO                    = "sudo ";
  public static final String SUDO_PASSWORD_PROMPT    = "[sudo] password for ";
  public static final String SUDO_PW_CHAR            = ":";
  public static final String STILL_PROCESSING_SUDO   = "Still processing ";
  public static final String USER_PROP_GET           = " UserPropGet";
  public static final String DASH_DASH_USER          = " --user ";
  public static final String COLON_SPACE             = ": ";
  public static final String OPEN_CURLY              = "{";
  public static final String CLOSE_CURLY             = "}";
  public static final String OPEN_SQUARE             = "[";
  public static final String CLOSE_SQUARE            = "]";
  public static final String OPEN_PAREN              = "(";
  public static final String CLOSE_PAREN             = ")";
  public static final String THREAD_SLEEP            = "Thread.sleep";
  public static final String FUTURE_CREDEL_RESPONSES = "futureCreateDeleteResponses";
  public static final String SVC_SUBMIT_CALLABLE_ST  = "service.submit(Callable<String[]>)";
  public static final String PASS_MASK               = "********";
  public static final String PASS_MASK_AINT          = "AintTellin";
  public static final String HOST_ERROR              = "ERROR:";
  public static final String RESPONSE_ERROR_MESSAGE  = "OpenVPN host server didn't respond, in time and/or with expected result. Please try again.";
  public static final String GET_USER_VALID_MASK     = "\"" + PASS_MASK + "\": {";
  public static final String TIMESTAMP_FORMAT        = "yyyy-MM-dd HH:mm:ss.SSS";

  public static final String PROMPT_POUND_SIGN   = "#";
  public static final String AT_SIGN             = "@";
  public static final String ROOT_USER           = "root";
  public static final String ROOT_AT             = "root@";


  @Value("${kajeet.nni.ssh.target.host}")
  private String host = "10.101.1.5";

  @Value("${kajeet.nni.ssh.target.user}")
  private String user = "UserMyUser";

  @Value("${kajeet.nni.ssh.target.pass}")
  private String pass = "CaptainMyCaptain";

  @Value("${kajeet.nni.ssh.target.port}")
  private int port = 42;

  @Value("${kajeet.nni.ssh.sacli.path}")
  private String sacliPath = "/usr/local/openvpn_as/scripts/sacli";

  @Autowired
  VpnConversationService pool;


  private Conversation getConversation() throws SystemException {
    ConversationStarter greeting = new ConversationStarter(user, pass, host, port);
    return pool.getConversation(greeting, 2);
  }

  public static String getBeginningAndEndForMaxLen (String maybeTooLongString, int maxLength) {
    if (maybeTooLongString.length()<maxLength) return maybeTooLongString;
    maybeTooLongString = maybeTooLongString.substring(ZERO, ((maxLength/TWO)-FIVE)) + MORE_CONTINUATION
      + maybeTooLongString.substring(maybeTooLongString.length() - ((maxLength/TWO)-FIVE));
    return maybeTooLongString;
  }

  static String waitForPrompt(ByteArrayOutputStream outputStream, String command, int waitTimeout, String promptChar, PrintStream stream, String user, String pass, String promptPhrase) throws InterruptedException {
    String methodName = "waitForPrompt";
    log.trace(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    log.trace("Command = " + command);
//    int retries = waitTimeout<ONE_THOUSAND?ONE_THOUSAND:ONE_HUNDRED;
    int retries = ONE_HUNDRED;
    String outputString = null; //new String(outputStream.toByteArray());
    int indexOf1stMagicChar = ZERO;
    String sudoPwPrompt = SUDO_PASSWORD_PROMPT + user + SUDO_PW_CHAR;
//    String searchString = ""+promptChar+command.substring(ZERO, THREE); // TRIED AND TRUE? BUT THEORETICALLY FLAWED
    String searchString = promptPhrase; //// EXPERIMENTAL 2021.06.03 EXPERIMENTAL 2021.06.03 INSTABILITY OBSERVED FOR PUT WITH POOL SIZE 1!!!!!
    String commandResponse = "";
    for (int x = ZERO; x < retries; x++) {
      log.trace(THREAD_SLEEP+OPEN_PAREN+waitTimeout+CLOSE_PAREN+SPACE+methodName+"[1]");
      Thread.sleep(waitTimeout); // Gives the host a chance to respond
      outputString = new String(outputStream.toByteArray());
      log.trace("outputString = " + outputString.replace(pass, PASS_MASK));
      log.trace("outputString.endsWith(sudoPwPrompt) == " + (outputString.length()>ZERO && outputString.substring(ZERO, outputString.length()-THREE).endsWith(sudoPwPrompt)));
      if (outputString.length()>ZERO && outputString.substring(ZERO, outputString.length()-THREE).endsWith(sudoPwPrompt)) {
        stream.println(pass); // Provide the password
        stream.flush();
        return waitForPrompt(outputStream, STILL_PROCESSING_SUDO + command, waitTimeout, promptChar, stream, user, pass, promptPhrase);
      }
      indexOf1stMagicChar = outputStream.toString().indexOf(searchString)+ONE;
      if (outputStream.toString().indexOf(promptChar, indexOf1stMagicChar) > ZERO) {
        outputString = new String(outputStream.toByteArray());
        outputStream.reset();

        // Wait until the above commands have finished processing
        for (int i=ZERO; i<ONE_HUNDRED; i++) {
          log.trace(THREAD_SLEEP+OPEN_PAREN+waitTimeout+CLOSE_PAREN+SPACE+methodName+"[2]");
          Thread.sleep(waitTimeout);
          commandResponse = new String(outputStream.toByteArray());

          if ((commandResponse.length() == ZERO)
//              && ((command.indexOf(USER_PROP_GET)>=ZERO && (command.indexOf(STILL_PROCESSING_SUDO)<ZERO))
//              && (outputString.lastIndexOf(promptChar)==outputString.length()-ONE))
             ) { // Nothing more returning in answer to the command
//          if (commandResponse.length()==ZERO) { // Nothing more returning in answer to the command
            break;
          }
          outputStream.reset();
          outputString += commandResponse;
        }
        if (x>FIFTY) {
          log.info(String.format(CLASS_NAME_DOT + WAIT_FOR_PROMPT + " finished after %s retries, each %s milliseconds.", x, waitTimeout)); // How many retries did it take?
          String outputLogMsg = getBeginningAndEndForMaxLen(outputString.replace(pass, PASS_MASK), ONE_THOUSAND);
          log.info(CLASS_NAME_DOT + WAIT_FOR_PROMPT + " returning: " + outputLogMsg);
        }
        break;
      }
      if (x==retries-ONE) {
        String outputLogMsg = getBeginningAndEndForMaxLen(outputString, ONE_THOUSAND);
        log.error(CLASS_NAME_DOT + WAIT_FOR_PROMPT
          + SPACE + x + " retries exhausted; inferring lost connection, throwing InterruptedException. Command = "
          + command + "; outputString = " + outputLogMsg);
        throw new InterruptedException("Retries exhausted in method " + methodName);
      }
    }
    log.trace(CLASS_NAME+" commandResponse/outputString = " + getBeginningAndEndForMaxLen(outputString.replace(pass, PASS_MASK), FIVE_HUNDRED));
    log.trace(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return outputString.replace(pass, PASS_MASK);
  }

  static String[] execHostCommands (List<String> commandsList, Conversation conversation, String user, String pass) throws SystemException {
    String methodName = "execHostCommands[1]";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    String[] commandArray = commandsList.toArray(String[]::new);
    log.info(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return execHostCommands (commandArray, conversation, user, pass);
  }

  static String[] execHostCommands (String[] commands, Conversation conversation, String user, String pass) throws SystemException {
    String methodName = "execHostCommands[2]";
    log.trace(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    String[] commandResponses = null;
    try {
      commandResponses = new String[commands.length];
      ByteArrayOutputStream responseStream   = null;
      PrintStream stream                     = null;
      log.trace("conversation = " + conversation);
      responseStream  = conversation.getResponseStream(); // May throw NullPointerException if connection fails
      String response = new String(responseStream.toByteArray());
      stream = conversation.getStream();
      for (int i=ZERO; i<commands.length; i++) {
        log.trace(CLASS_NAME_DOT + methodName + COLON_SPACE + commands[i]);
        stream.println(commands[i]);
        stream.flush();
        commandResponses[i] = waitForPrompt(responseStream, commands[i], ONE_HUNDRED, conversation.getPromptChar(), stream, user, pass, conversation.getPromptPhrase());
      }
    } catch (InterruptedException x) {
      log.error("InterruptedException in " + methodName + COLON_SPACE + x.getMessage());
      throw new SystemException(x.getMessage()); // TESTING COMMENTED OUT
    } catch (NullPointerException x) {
      log.error("NullPointerException in " + methodName + COLON_SPACE + x.getMessage());
      throw new SystemException("Unable to communicate with OpenVPN host server; Please try again later.");
    }
    log.trace(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return commandResponses;
  }


  VpnUserInfoMap execGetUserInfo (String userName) throws SystemException {
    String methodName = "execGetUserInfo";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    Conversation conversation = getConversation();

    String[] commands = new String[ONE];
    commands[ZERO] = SUDO + sacliPath + " --pfilt " + userName + USER_PROP_GET;
    String[] commandResponses = execHostCommands(commands, conversation, user, pass);
    String result = null;
    ObjectMapper mapper = new ObjectMapper();
    VpnUserInfoMap vpnUserInfoMap = new VpnUserInfoMap();
    try {
      result = commandResponses[ZERO].substring(commandResponses[ZERO].indexOf(OPEN_CURLY), commandResponses[ZERO].lastIndexOf(CLOSE_CURLY) + ONE);
      if (result.indexOf(GET_USER_VALID_MASK.replace(PASS_MASK, userName))<ZERO) throw new Exception("UserName not echoed by host.");
      String userInfoString = result.substring(result.indexOf(OPEN_CURLY, ONE), result.indexOf(CLOSE_CURLY)+ONE);
      vpnUserInfoMap = mapper.readValue(result, VpnUserInfoMap.class);
      log.trace("vpnUserInfoMap = " + mapper.writeValueAsString(vpnUserInfoMap));
    } catch (StringIndexOutOfBoundsException x) {
      log.error(methodName + " threw StringIndexOutOfBoundsException: " + x.getMessage() + COLON_SPACE + commandResponses[ZERO]);
      throw new SystemException(RESPONSE_ERROR_MESSAGE);
    } catch (JsonMappingException x) {
      log.error(methodName + " threw JsonMappingException: " + x.getMessage() + COLON_SPACE + commandResponses[ZERO]);
      throw new SystemException(RESPONSE_ERROR_MESSAGE);
    } catch (JsonProcessingException x) {
      log.error(methodName + " threw JsonProcessingException: " + x.getMessage() + COLON_SPACE + commandResponses[ZERO]);
      throw new SystemException(RESPONSE_ERROR_MESSAGE);
    } catch (Exception x) {
      log.error(methodName + " threw Exception: " + x.getMessage() + COLON_SPACE + commandResponses[ZERO]);
    } finally {
      if (conversation != null) conversation.release();
    }
    log.info(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return vpnUserInfoMap;
  }

  VpnUserInfoMap execGetAllUsersInfo (Conversation conversation) throws SystemException {
    String methodName = "execGetAllUsersInfo";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    String[] commands = new String[ONE];
    ObjectMapper mapper = new ObjectMapper();
    commands[ZERO] = SUDO + sacliPath + USER_PROP_GET;
    String[] commandResponses = execHostCommands(commands, conversation, user, pass);
    String result = null;
    VpnUserInfoMap vpnUserInfoMap = new VpnUserInfoMap();
    try {
      result = commandResponses[ZERO].substring(commandResponses[ZERO].indexOf(OPEN_CURLY), commandResponses[ZERO].lastIndexOf(CLOSE_CURLY) + ONE);
      vpnUserInfoMap = mapper.readValue(result, VpnUserInfoMap.class);
      log.trace("vpnUserInfoMap = " + mapper.writeValueAsString(vpnUserInfoMap));
    } catch (StringIndexOutOfBoundsException x) {
      log.error(methodName + " threw StringIndexOutOfBoundsException: " + x.getMessage());
      throw new SystemException(RESPONSE_ERROR_MESSAGE);
    } catch (JsonMappingException x) {
      log.error(methodName + " threw JsonMappingException: " + x.getMessage());
      throw new SystemException(RESPONSE_ERROR_MESSAGE);
    } catch (JsonProcessingException x) {
      log.error(methodName + " threw JsonProcessingException: " + x.getMessage());
      throw new SystemException(RESPONSE_ERROR_MESSAGE);
    } catch (Exception x) {
      log.error(methodName + " threw Exception: " + x.getMessage());
    }
    log.info(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return vpnUserInfoMap;
  }

  VpnUserInfoMap execGetAllUsersInGroupInfo (Conversation conversation, String groupName) throws SystemException {
    String methodName = "execGetAllUsersInGroupInfo";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    VpnUserInfoMap allUsersInfoMap = execGetAllUsersInfo (conversation);
    VpnUserInfoMap existingGroupUserMap = new VpnUserInfoMap();

    existingGroupUserMap = allUsersInfoMap.keySet().stream()
      .filter(key -> groupName.equals(allUsersInfoMap.get(key).getConn_group()))
                        .collect(Collectors.toMap(
                          Function.identity(),
                          allUsersInfoMap::get,
                          (existing, replacement) -> existing, // Moot since already unique
                          VpnUserInfoMap::new
                        ));

    log.info(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return existingGroupUserMap;
  }


  VpnUserInfoMap execDeleteUser (String userName) throws SystemException {
    String methodName = "execDeleteUser";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    Conversation conversation = getConversation();
    String[] commands = new String[ONE];
    commands[ZERO] = SUDO + sacliPath + DASH_DASH_USER + userName + " UserPropDelAll";
//    commands[ZERO] = SUDO + sacliPath + DASH_DASH_USER + userName + " UserPropDelAllXXXX"; // DELIBERATE INVALID COMMAND FOR DEV/TESTING PURPOSES 2021.05.25
    String[] commandResponses = execHostCommands(commands, conversation, user, pass);
    VpnUserInfoMap userInfoMap = execGetUserInfo(userName);

    if (conversation != null) conversation.release();
    log.info(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return userInfoMap;
  }

  VpnUserInfoMap execCreateUser (String userName, String groupName) throws SystemException {
    String methodName = "execCreateUser";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    Conversation conversation = getConversation();
    String[] commands = new String[2];
    commands[ZERO] = SUDO + sacliPath + " --user \"" + userName + "\" --key \"type\" --value \"user_connect\" UserPropPut";
    commands[ONE] = SUDO + sacliPath + " --user \"" + userName + "\" --key \"conn_group\" --value \"" + groupName + "\" UserPropPut";
    String[] commandResponses = execHostCommands(commands, conversation, user, pass);

    VpnUserInfoMap userInfoMap = execGetUserInfo(userName);

    if (conversation != null) conversation.release();
    log.info(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    return userInfoMap;
  }

  public VpnUserInfoMap execAuditGroupUsers (String groupName, VpnGroupUsers payload) throws SystemException {
    String methodName = "execAuditGroupUsers";
    log.info(methodName + START + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
//    ConversationStarter greeting = new ConversationStarter(user, pass, host, port);
    Conversation[] conversations = null;
    Conversation conversation1 = getConversation();
    String[] getCommand = new String[ONE];
    getCommand[ZERO] = SUDO + sacliPath + USER_PROP_GET;
    ObjectMapper mapper = new ObjectMapper();
    VpnUserInfoMap resultGroupUserMap = null;
     try {
      VpnUserInfoMap existingGroupUserMap = execGetAllUsersInGroupInfo(conversation1, groupName);
      if (conversation1 != null) conversation1.release();

      try {
        log.trace("existingGroupUserMap = " + mapper.writeValueAsString(existingGroupUserMap));
      } catch (JsonProcessingException x) {
        log.error(methodName + " threw JsonProcessingException: " + x.getMessage());
      }

      Map<String, String> payloadUserMap = payload.getUserMap();
       List<String> createDeleteUserCommandList = new ArrayList<>();
       List<String> createDeleteUserCommandBatch = new ArrayList<>();

       for (String existingUser : existingGroupUserMap.keySet()) {
        if (payloadUserMap.get(existingUser) == null) {
          createDeleteUserCommandList.add(SUDO + sacliPath + DASH_DASH_USER + existingUser + " --key \"conn_group\" UserPropDel");
        }
      }
      for (String putUser : payloadUserMap.keySet()) {
        if (existingGroupUserMap.get(putUser) == null) {
          createDeleteUserCommandList.add(SUDO + sacliPath + DASH_DASH_USER + putUser + " --key \"conn_group\" --value \"" + groupName + "\" UserPropPut");
        }
      }
      log.info(createDeleteUserCommandList.size() + " add/remove from group commands to execute...");

      if (createDeleteUserCommandList.size()>0) {
        conversations = new Conversation[pool.getPoolSize()];
        String[] createDeleteCommandResponses = new String[pool.getPoolSize()];
        Future<String[]>[] futureCreateDeleteResponses = new Future[pool.getPoolSize()];
        ExecutorService service = null;
//      String[] createDeleteCommandArray = createDeleteUserCommandList.toArray(String[]::new);
        try {
          int totalNumCommands = createDeleteUserCommandList.size();
          int ratioPoolCommands = totalNumCommands/pool.getPoolSize();
          int remainder = (totalNumCommands%pool.getPoolSize());
          int numCommandsPerCall = ratioPoolCommands + (remainder>0?1:0);

          // Prep command batches
          Stack<String> createDeleteUserCommandStack = new Stack();
          createDeleteUserCommandStack.addAll(createDeleteUserCommandList);
//          List<String>[] listOfCreateDeleteUserCommandBatches = new ListString[pool.getPoolSize()];
          Stack<List<String>> stackOfCreateDeleteUserCommandBatches = new Stack<>();
          String commandString = null;
          for (int x = 0; x < pool.getPoolSize(); x++) {
            createDeleteUserCommandBatch = new ArrayList<>(); // Reset
            for (int y = 0; y < numCommandsPerCall; y++) {
              if (!createDeleteUserCommandStack.empty()) {
                commandString = createDeleteUserCommandStack.pop();
                if (commandString != null) {
                  createDeleteUserCommandBatch.add(commandString);
                } else {
                  break;
                }
              }
            }
            if (createDeleteUserCommandBatch.size()>0) {
              stackOfCreateDeleteUserCommandBatches.push(createDeleteUserCommandBatch);
            }
          }
          // End Pred command batches

          // Process batches of commands
          conversations = new Conversation[pool.getPoolSize()];
          service = Executors.newFixedThreadPool(pool.getPoolSize());
          while (!stackOfCreateDeleteUserCommandBatches.empty()) {
//          listOfCommandsToExecute = new ArrayList<>(); // reset
            for (int x = 0; x < pool.getPoolSize(); x++) {
              Conversation conversation = getConversation();
              conversations[x] = conversation;
              if (!stackOfCreateDeleteUserCommandBatches.empty()) {
                List<String> listOfCommandsToExecute = stackOfCreateDeleteUserCommandBatches.pop();
                if (listOfCommandsToExecute != null) {
                  Callable<String[]> callThis = () -> execHostCommands(listOfCommandsToExecute, conversation, user, pass);
                  log.info(FUTURE_CREDEL_RESPONSES + OPEN_SQUARE + x + CLOSE_SQUARE + EQUALS + SVC_SUBMIT_CALLABLE_ST + START + conversation);
                  futureCreateDeleteResponses[x] = service.submit(callThis);
                  log.info(FUTURE_CREDEL_RESPONSES + OPEN_SQUARE + x + CLOSE_SQUARE + EQUALS + SVC_SUBMIT_CALLABLE_ST + END + conversation);
                  log.info("futureCreateDeleteResponses[" + x + "] = service.submit(callThis); [END] " + conversation);
                } else {
                  break;
                }
              }
            }
//            for (int x = 0; x < pool.getPoolSize(); x++) {
              int index = -1;
              boolean done = true;
              int timeoutSafetyValve = 0;
              do {
                index = -1;
                log.trace ("futureCreateDeleteResponses.length = " + futureCreateDeleteResponses.length);
                for (Future<String[]> futureString : futureCreateDeleteResponses) {
                  if (futureString==null) break;
                  index++;
                  timeoutSafetyValve++;
                  log.info("CommandResponses = futureString.get(FIVE, TimeUnit.SECONDS); [START]");
                  try {
                    createDeleteCommandResponses = futureString.get(FIVE, TimeUnit.SECONDS);
                    done = true;
                    log.info("CommandResponses = futureString.get(FIVE, TimeUnit.SECONDS); [END]: " + futureString);
                    if (conversations[index] != null) conversations[index].release();
                  } catch (TimeoutException ex) {
                    log.info("futureString.get() NOT DONE YET.");
                    done = false;
                  }
                }
                if (done) break;
                if (timeoutSafetyValve > (createDeleteUserCommandList.size()) * TEN) {
                  log.error("Exhausted attempts to retrieve command results");
                  throw new TimeoutException("Exhausted attempts to retrieve command results");
                }
              } while (!done);
              for (Conversation conversation : conversations) {
                if (conversation != null) conversation.release();
              }
//            }
          } // end loop/Next batch
        } catch (TimeoutException x) {
          throw new SystemException("Could not complete delete/create user commands on VPN host. [TimeoutException] " + x.getMessage());
        } catch (ExecutionException x) {
          throw new SystemException("Could not complete delete/create user commands on VPN host. [ExecutionException] " + x.getMessage());
        } catch (InterruptedException x) {
          throw new SystemException("Could not complete delete/create user commands on VPN host. [InterruptedException] " + x.getMessage());
        } catch (Exception x) {
          log.error(methodName + " threw Exception: " + x.getMessage());
          throw x;
        } finally {
          if (service != null) service.shutdown();
        }
      }
      conversation1 = getConversation();
      resultGroupUserMap = execGetAllUsersInGroupInfo(conversation1, groupName);

//    if (conversation != null) conversation.release();
      log.info(methodName + END + LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT)));
    } finally {
      if (conversation1 != null) conversation1.release();
    }
    return resultGroupUserMap;
  }

  @Override
  public VpnUserInfoMap getUserInfo(String userName, String dummy, Performance callStats) throws LoginException, InvalidInputException, SystemException {
    String info = "WEB-3841: NniVpnAdaptorHostService: User details for " + userName + " ";
    log.trace(info);
    if (callStats != null) callStats.setAsr_sent(WebLogger.nowTimestamp());
    VpnUserInfoMap userInfoMap = execGetUserInfo(userName);
    if (callStats != null) callStats.setAsr_completed(WebLogger.nowTimestamp());
    return userInfoMap;
  }

  @Override
  public VpnUserInfoMap createUserInGroup(String userName, String groupName, Performance callStats) throws LoginException, InvalidInputException, SystemException {
    String info = "WEB-3839: NniVpnAdaptorHostService: Create User " + userName + " in Group " + groupName;
    log.trace(info);
    if (callStats != null) callStats.setAsr_sent(WebLogger.nowTimestamp());
    VpnUserInfoMap userInfoMap = execCreateUser(userName, groupName);
    if (callStats != null) callStats.setAsr_completed(WebLogger.nowTimestamp());
    return userInfoMap;
  }

  @Override
  public VpnUserInfoMap auditGroupUsers(String groupName, VpnGroupUsers payload, Performance callStats) throws LoginException, InvalidInputException, SystemException {
    String info = "WEB-3831: NniVpnAdaptorHostService: Audit Group " + groupName;
    log.trace(info);
    if (callStats != null) callStats.setAsr_sent(WebLogger.nowTimestamp());
    VpnUserInfoMap userInfoMap = execAuditGroupUsers(groupName, payload); // Overwrite not concatenate
    if (callStats != null) callStats.setAsr_completed(WebLogger.nowTimestamp());
    return userInfoMap;
  }

  @Override
  public VpnUserInfoMap deleteUser(String userName, String dummy, Performance callStats) throws LoginException, InvalidInputException, SystemException {
    String info = "WEB-3840: NniVpnAdaptorHostService: Delete User " + userName;
    log.trace(info);
    if (callStats != null) callStats.setAsr_sent(WebLogger.nowTimestamp());
    VpnUserInfoMap userInfoMap = execDeleteUser(userName);
    if (callStats != null) callStats.setAsr_completed(WebLogger.nowTimestamp());
    return userInfoMap;
  }

  @Override
  public VpnUserInfoMap getGroupUserInfo(String groupName, String dummy, Performance callStats) throws LoginException, InvalidInputException, SystemException {
    String info = "getGroupUserInfo" + OPEN_PAREN + groupName + CLOSE_PAREN;
    log.info(info);
    if (callStats != null) callStats.setAsr_sent(WebLogger.nowTimestamp());
    Conversation conversation = getConversation();
    VpnUserInfoMap userInfoMap = null;
    try {
      userInfoMap = execGetAllUsersInGroupInfo(conversation, groupName);
    } finally {
      if (conversation != null) conversation.release();
      if (callStats != null) callStats.setAsr_completed(WebLogger.nowTimestamp());
    }
    return userInfoMap;
  }

}
