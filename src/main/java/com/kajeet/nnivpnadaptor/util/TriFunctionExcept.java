package com.kajeet.nnivpnadaptor.util;

import com.kajeet.nnivpnadaptor.exception.InvalidInputException;
import com.kajeet.nnivpnadaptor.exception.LoginException;
import com.kajeet.nnivpnadaptor.exception.SystemException;

@FunctionalInterface
public interface TriFunctionExcept<T, U, V, R> {
	
	R apply(T t, U u, V v) throws LoginException, InvalidInputException, SystemException;
	
}
