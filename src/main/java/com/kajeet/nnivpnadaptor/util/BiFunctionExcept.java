package com.kajeet.nnivpnadaptor.util;

import com.kajeet.nnivpnadaptor.exception.InvalidInputException;
import com.kajeet.nnivpnadaptor.exception.LoginException;
import com.kajeet.nnivpnadaptor.exception.SystemException;

@FunctionalInterface
public interface BiFunctionExcept<T, U, R> {
	R apply(T t, U u) throws LoginException, InvalidInputException, SystemException;
}
